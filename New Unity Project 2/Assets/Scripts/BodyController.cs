﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyController :  MonoBehaviour, IVec {

	private float speed;
	public GameObject body;
	public GameObject parent;

	float currentTrav;
	float trav;
	Vector2 currentVec;
	Vector2 pos;
	Vector2 prevVec;
	Vector2 nextVec;
	GameObject obj;
	bool spawn;

	public Vector2 cVec()
	{
		 return prevVec; 
	}

	public float sp()
	{
		return speed;
	}

	public void Init(Vector2 curVec, Vector2 nVec, float sp)
	{
		pos = transform.position;
		currentVec = curVec;
		nextVec = nVec;
		speed = sp;
		currentTrav = (float)(3.6 * speed);
		trav = 0;
	}

	void FixedUpdate()
	{
		
		nextVec = parent.GetComponent<IVec> ().cVec ();
		speed = parent.GetComponent<IVec> ().sp ();
		if (trav < 3.6) {
			gameObject.transform.Translate (new Vector3(currentVec.x,currentVec.y, 0) * currentTrav * Time.deltaTime);
			trav += Time.deltaTime * currentTrav;
		} 
		else 
		{
			pos += currentVec * 3.6f;
			transform.position = pos;
			trav = 0;
			prevVec = currentVec;
			currentVec = nextVec;
			currentTrav = (float)(3.6 * speed);
			if (obj != null)
			{
				if(spawn)
				{
					obj.transform.position = transform.position;
					obj.GetComponent<BodyController> ().Init (currentVec, nextVec, speed);
					spawn = false;
				}
				else
				{
					obj.SetActive (true);
				}
			}
		}
	}

	public void Spawn()
	{
		if (obj == null) 
		{
			obj = Instantiate (body).gameObject;
			spawn = true;
			obj.GetComponent<BodyController> ().parent = gameObject;
			obj.SetActive (false);
		} 
		else
		{
			obj.GetComponent<BodyController> ().Spawn ();
		}
	}

	public void Stop()
	{
		if (obj != null) {
			obj.GetComponent<BodyController> ().Stop ();
			obj = null;
		}
		Destroy (gameObject);
	}
}
