﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController :  MonoBehaviour, IVec {

	public float speed;
	public Text countText;
	public Text maxCountText;
	public Text winText;
	public GameObject body;
	public Button button;

	float currentTrav;
	float trav;
	Vector2 currentVec;
	Vector2 prevVec;
	Vector2 nextVec;
	Vector2 pos;
	private int count;
	private int maxCount = 0;
	bool spawn;
	public GameObject obj;

	public Vector2 cVec()
	{
		 return prevVec; 
	}

	public float sp()
	{
		return speed;
	}

	void Init()
	{
		transform.position = Vector2.zero;
		pos = Vector2.zero;
		obj = null;
		count = 0;
		winText.text = "";
		SetCountText ();
		currentVec = Vector2.up;
		nextVec = Vector2.up;
		prevVec = currentVec;
		currentTrav = (float)(3.6 * speed);
		trav = 0;
	}

	void Start()
	{
		Init ();
	}

	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		if (trav != 0 && moveHorizontal != 0 && (currentVec == Vector2.up || currentVec == Vector2.down)) 
		{
			nextVec = new Vector2(moveHorizontal, 0 );
			nextVec.Normalize();
		} 
		else if (trav != 0 && moveVertical != 0 && (currentVec == Vector2.left || currentVec == Vector2.right)) 
		{
			nextVec = new Vector2(0,moveVertical);
			nextVec.Normalize();
		}
		if (trav < 3.6) {
			gameObject.transform.Translate (new Vector3(currentVec.x,currentVec.y, 0) * currentTrav * Time.deltaTime);
			trav += Time.deltaTime * currentTrav;
		} 
		else 
		{
			pos += currentVec * 3.6f;
			transform.position = pos;
			trav = 0;
			prevVec = currentVec;
			currentVec = nextVec;
			currentTrav = (float)(3.6 * speed);
			if (obj != null)
			{
				if(spawn)
				{
					obj.transform.position = transform.position;
					obj.GetComponent<BodyController> ().Init (currentVec, nextVec, speed);
					spawn = false;
				}
				else
				{
					obj.SetActive (true);
				}
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag ("PickUp")) 
		{
			other.gameObject.GetComponent<Rotator>().PickUps();
			count++;
			if (count >= maxCount) 
			{
				maxCount = count;
			}
			speed += 0.1f;
			SetCountText ();
			if (obj == null) {
				obj = Instantiate (body).gameObject;
				spawn = true;
				obj.GetComponent<BodyController> ().parent = gameObject;
				obj.SetActive (false);
			} else {
				obj.GetComponent<BodyController> ().Spawn ();
			}
		}
		else if (other.gameObject.CompareTag ("Body") && other.gameObject != obj)
		{
			winText.text = "You lose!";
			obj.SetActive (true);
			obj.GetComponent<BodyController> ().Stop ();
			obj = null;
			Init ();
			gameObject.SetActive (false);
			button.gameObject.SetActive (true);
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		winText.text = "You lose!";
		if (obj != null) {
			obj.GetComponent<BodyController> ().Stop ();
			obj = null;
		}
		gameObject.SetActive (false);
		Init ();
		button.gameObject.SetActive (true);
	}

	void SetCountText()
	{
		countText.text = "Count: " + count.ToString ();
		maxCountText.text = "Max count: " + maxCount.ToString ();
	}

}
