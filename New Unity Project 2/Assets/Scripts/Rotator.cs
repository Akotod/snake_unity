﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

	void Update () {
		transform.Rotate (new Vector3(0, 0, 45) * Time.deltaTime);
	}

	public void PickUps()
	{
		transform.position = new Vector3 (Random.Range(-3,3)*3.6f,Random.Range(-3,3)*3.6f,0);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag ("Body"))
		{
			PickUps ();
		}
	}
}
